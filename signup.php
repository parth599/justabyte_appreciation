<?php include 'db.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles/signup.css">
        <style>
                body {font-family: Arial, Helvetica, sans-serif;}
                * {box-sizing: border-box;}
                
                /* Full-width input fields */
                </style>
           /*     <script>
                function registration()
	{

		var name= document.getElementById("t1").value;
		var email= document.getElementById("t2").value;
		var uname= document.getElementById("t3").value;
		var pwd= document.getElementById("t4").value;			
		var cpwd= document.getElementById("t5").value;
		
        //email id expression code
		var pwd_expression = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/;
		var letters = /^[A-Za-z]+$/;
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if(name=='')
		{
			alert('Please enter your name');
		}
		else if(!letters.test(name))
		{
			alert('Name field required only alphabet characters');
		}
		else if(email=='')
		{
			alert('Please enter your user email id');
		}
		else if (!filter.test(email))
		{
			alert('Invalid email');
		}
		else if(uname=='')
		{
			alert('Please enter the user name.');
		}
		else if(!letters.test(uname))
		{
			alert('User name field required only alphabet characters');
		}
		else if(pwd=='')
		{
			alert('Please enter Password');
		}
		else if(cpwd=='')
		{
			alert('Enter Confirm Password');
		}
		else if(!pwd_expression.test(pwd))
		{
			alert ('Upper case, Lower case, Special character and Numeric letter are required in Password filed');
		}
		else if(pwd != cpwd)
		{
			alert ('Password not Matched');
		}
		else if(document.getElementById("t5").value.length < 6)
		{
			alert ('Password minimum length is 6');
		}
		else if(document.getElementById("t5").value.length > 12)
		{
			alert ('Password max length is 12');
		}
		else
		{				                            
               alert('Thank You for Login & You are Redirecting to login page');
			   // Redirecting to other page or webste code. 
			   window.open("login.php");}
return true;
	}
                </script>*/
    </head>

<body>
       
  <form class="modal-content" method="post" action="process.php" >
    <div class="container">
      <h1>Sign Up</h1>
      <p>Please fill in this form to create an account.</p>
      <hr>
      <label><b>CID*</b></label>
      <input type="text" name="cid" placeholder="Enter CID number"  required>

      <label><b>First Name*</b></label>
      <input type="text" name="firstname" placeholder="Enter First Name" id="t1" required>

      <label><b>Last Name*</b></label>
      <input type="text" placeholder="Enter Last Name"  name="lastname" required>

      <label><b>Date of birth*</b></label>
      <input type="date"  name="dob" required><br><br><br>

      <label><b>Date of joining*</b></label>
      <input type="date" name="doj" required><br>
      <br>
      <br>
      <label><b>Gender*</b></label>    
      <input type="radio" name="gender">Male <input type="radio" name="abc">Female  <br>
      <br>
      <br>
      <label><b>Email*</b></label>
      <input type="text" placeholder="Enter Email" id="t2" name="email" required>

      <label><b>Post*</b></label>
      <input type="text" placeholder="Enter Post"  name="post" required>

      <label><b>Hobbies*</b></label>
      <input type="text" name="hobbies" placeholder="Enter Hobbies" required>

      <label><b>Enter Mobile number*</b></label>
      <input type="text" placeholder="Enter mobile number"  name="mobile" required>

      <label><b>Enter Username*</b></label>
      <input type="text" placeholder="Enter username" id="t3" name="username" required>

      <label><b>Password*</b></label>
      <input type="password" name="password" placeholder="Enter Password"  id="t4"  required>

      <label><b>Repeat Password*</b></label>
      <input type="password" name="rpassword" placeholder="Repeat Password" id ="t5"  required>
      
      <label>
        <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
      </label>

      <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

      <div class="clearfix">
                <button type="submit" class="signupbtn"> Sign Up</button>
      </div>
    </div>
  </form>
</div>



</body>
</html>
