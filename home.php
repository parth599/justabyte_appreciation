
<!DOCTYPE html>
<html lang="en">

<head>
       <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Great+Vibes|Noticia+Text" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="styles/style.css">

<style>

.details,h2,p{
margin-left:10px;
}
body{
	margin:0;
	padding:0;
	background:#fff;
	font-family: 'Open Sans', sans-serif;
 }
h3{
	margin:0;	
	font-family: 'Montserrat', sans-serif;
}	
#dolor{
  height:700px;
}
p{
	margin:0;
}
ul{
	margin:0;
	padding:0;
}
p,ul{
	font-size:14px;
}
.agileits-banner-info h3 {
    color: #fff;
    font-size: 4em;
    margin: 0.1em 0 0.2em 0;
    letter-spacing: 2px;
    text-transform: uppercase;
    font-weight: 700;
	
	
}
.agileits-banner-info {
    text-align: center;
    padding: 18em 0 0em;
}
.agileits-banner-info p {
    font-size: 1em;
    line-height: 2.2em;
    letter-spacing: 2px;

    width: 49%;
	margin: 0 auto;
    text-align:  center;
}
.w3layouts-banner-top{
    background: url(images/img1.jpg) no-repeat 0px 0px;
background-size: 1500px 300px;
background-color:orange;
min-height:300px;
}
.w3layouts-banner-top1{
	background: url(images/img2.jpg) no-repeat 0px 0px;
    background-size: 1500px 300px;
	background-color:orange;
	min-height:300px;
}
.w3layouts-banner-top2{
	background: url(images/img3.jpeg) no-repeat 0px 0px;
    background-size: 1500px 300px;
	background-color:orange;
	min-height:300px;
}
.w3layouts-banner-top3{
	background: url(images/img4.jpg) no-repeat 0px 0px;
  background-size: 1500px 300px;
	background-color:orange;
	min-height:300px;
}
@media (max-width:640px){
h3 {
	margin-top: 0px;
	margin-bottom: 0px;
}
}
@media (max-width:480px){
p {
	font-size: 14px;
}
}
@media (max-width: 320px){
p {
	font-size: 13px;
}
}
@media (max-width:1440px){
	.agileits-banner-info {
		padding: 16em 0 0em;
	}
	.w3layouts-banner-top,.w3layouts-banner-top1,.w3layouts-banner-top2,.w3layouts-banner-top3{
		min-height:650px;
	}
	.agileits-banner-info h3 {
		font-size: 5.5em;
		margin: 0.1em 0 0.2em 0;
	}
}
@media (max-width:1280px){
    .w3layouts-banner-top,.w3layouts-banner-top1,.w3layouts-banner-top2,.w3layouts-banner-top3{
		min-height:600px;
	}
	.agileits-banner-info {
		padding: 14em 0 0em;
	}
	.agileits-banner-info h3 {
		font-size: 5em;
		margin: 0.1em 0 0.2em 0;
	}
}
@media (max-width:1080px){
	.agileits-banner-info h3 {
		font-size: 4em;
		margin: 0.1em 0 0.2em 0;
	}
}
@media (max-width:1050px){
	.w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height: 530px;
	}
	.agileits-banner-info p {
		font-size: 0.9em;
		line-height: 2.2em;
		width: 51%;
	}

	.agileits-banner-info h3 {
		font-size:3.5em;
		margin: 0.1em 0 0.2em 0;
	}
	.agileits-banner-info {
		padding: 13em 0 0em;
	}
}
@media (max-width:1024px){
	.agileits-banner-info {
		padding: 13em 0 0 2em;
	}
	.w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height:500px;
	}
}
@media (max-width:991px){
	.agileits-banner-info {
		padding: 11em 0 0 0em;
	}
	.agileits-banner-info p {
		font-size: 0.9em;
		line-height: 2.2em;
		width: 65%;
	
	}
.agileits-banner-info h3 {
    font-size: 3em;
}
}
@media (max-width:800px){
	.agileits-banner-info {
		padding: 11em 0 0 2em;
	}
}
@media (max-width:768px){
	.w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height:450px;
	}
		.agileits-banner-info {
		padding: 10em 0 0 3em;
	}
}
@media (max-width:667px){
	.w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height: 400px;
	}
    .agileits-banner-info h3 {
		font-size:2.5em;
		margin: 0.1em 0 0.2em 0;
	}
	.agileits-banner-info p {
		line-height: 2em;
		 width: 86%;
	}
	.agileits-banner-info {
		padding: 8em 0 0 3em;
	}
}
@media (max-width:640px){
    .w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height:350px;
	}
	.agileits-banner-info {
		padding: 7em 0 0 3em;
	}

}
@media (max-width:600px){
	.agileits-banner-info {
		padding: 6em 0 0 3em;
	}
}
@media (max-width:568px){

	.agileits-banner-info h3 {
		font-size: 2em;
		margin: 0.1em 0 0.2em 0;
	}
}
@media (max-width:480px){
	.w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height: 300px;
	}

	.agileits-banner-info h3 {
		font-size: 2em;
		margin: 0.1em 0 0.2em 0;
	}

	.agileits-banner-info {
		padding: 4em 0 0 3em;
	}
}
@media (max-width:414px){
	.agileits-banner-info p {
		line-height: 1.9em;
		width: 100%;
	}
	.agileits-banner-info {
		padding: 4em 0 0 2em;
	}
	.w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height:280px;
	}
}

.jislider__controller
{
margin-top:-90px;
}
@media (max-width:300px){
	.agileits-banner-info h3 {
    font-size: 1.8em;
}
}
@media (max-width:300px){
	.agileits-banner-info h3 {
		font-size: 1.7em;
		margin: 0.1em 0 0.2em 0;
	}
}
@media (max-width:300px){
	.agileits-banner-info {
		padding: 3em 0 0 1em;
	}
	.agileits-banner-info p {
		line-height: 1.9em;
		font-size: 0.8em;
	}
	.w3layouts-banner-top, .w3layouts-banner-top1, .w3layouts-banner-top2, .w3layouts-banner-top3 {
		min-height: 250px;
	}
	.agileits-banner-info h3 {
		font-size: 1.5em;
		margin: 0.1em 0 0.2em 0;
	}
}

</style>
</head>

<body style="background:orange;">

<div class="contain"> 
                    <h1 style="color:white;">Staff Appreciation/Just a Byte.com</h1>
               </div>
               <nav class="navbar navbar-inverse">
                 <div class="container-fluid">
                   
                   <ul class="nav navbar-nav">
                     <li class="active"><a href="">Home</a></li>
                     <li><a href="appreciate.php">Appreciate</a></li>
                     <li><a href="events.php">Events</a></li>
                     <li><a href="#">Chat</a></li>
                     <li class="log"><a href="login.php">Login</a></li>
                     <li class="sign"><a href="signup.php">Signup</a></li>
                   </ul>
                 </div>
               </nav>



<div class="outerbox">
            <div class="sliderbox">

                <div class="cover2">
                    <img src="images/img1.jpg" alt="" class="c1">
                </div>
                
                <div class="cover1">
                    <img src="images/img2.jpg" alt=""class="c2">
                </div>

                <div class="cover3">
                    <img src="images/img3.jpeg" alt=""class="c3">
                </div>
		<div class="cover4">
                    <img src="images/img4.jpg" alt=""class="c4">
                </div>

              

               
            </div>
        </div>
<div style="background:wheat;padding:10px;">
<h2 style="text-align:center">Events</h2>
</div>
	
       <div style="width:900px;height:250px;margin-left:15%;margin-top:20px;margin-bottom:50px;background:lightgrey;" >
	<marquee direction="up" motiondelay:10ms>
		<div>
			<div style="background:blue">
			<img src="images/music.jpg" width="400" style=" margin-left:250px;"  >
			</div>
		</div>
		<div>
			<h4 style="text-align:center">Musical Event</h4>
		</div>

		<div>
			<div style="background:red">
			<img src="images/dance.jpeg" width="400" style=" margin-left:250px;"  >
			</div>
		</div>
		<div>
			<h4 style="text-align:center">Dance Event</h4>
		</div>

		<div>
			<div style="background:pink">
			<img src="images/sports.png" width="400" style=" margin-left:250px;"  >
			</div>
		</div>
		<div>
			<h4 style="text-align:center">Sports Event</h4>
		</div>

		<div>
			<div style="background:purple">
			<img src="images/snd.jpg" width="400" style=" margin-left:250px;"  >
			</div>
		</div>
		<div>
			<h4 style="text-align:center">Sand Art</h4>
		</div>
	</marquee>
	<div style="height:100px;text-align:center" class="marq">
	<h2>Events</h2>
	</div>
</div>
<div style="background:wheat;padding:5px;">
<h2 style="text-align:center">Employee of the week!!</h2>
</div>
<div style="width:900px;height:370px;margin-left:15%;margin-top:20px;margin-bottom:50px;background:lightgrey;">
	<img src="images/emp.gif" style="border-radius:5%;" height="170px" class="details">
<h2 style="text-align:center">Details:</h2>
<p style="text-align:justify;margin-left:30%;"><b>Name:</b>Abc</p>
<p style="text-align:justify;margin-left:30%;"><b>Post:</b>HOD of Computer Science Department</p>
<p style="text-align:justify;margin-left:30%;"><b>Qualification:</b>PHD in Data Mining</p>
<p style="text-align:justify;margin-left:30%;"><b>Teaching Experience:</b>8 yers degree + 10 years diploma</p>
<p style="text-align:justify;margin-left:30%;"><b>Hobbies:</b>Music and reading books</p>
<p style="text-align:justify;margin-left:30%;"><b>Specialized in:</b> Computer Graphics & Data Mining</p>
</div>



</body>

</html>


